<?php

namespace App\Form;

use App\Entity\Admin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\CallbackTransformer;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email')
            ->add('roles', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices'  => [
                  'User' => 'ROLE_USER',
                  'Administrateur' => 'ROLE_ADMIN',
                  'Super User' => 'ROLE_SUPER_USER',
                ],
                // 'multiple'  => true, 
            ])
            // ->add('roles',  ChoiceType::class, [
            //     'choices' => [
            //         'Main Statuses' => [
            //             'Yes' => 'stock_yes',
            //             'No' => 'stock_no',
            //         ],
            //     ],
            // ])
            // ->add('roles',  EntityType::class, [
            //         'class' => Admin::class,
            //         // 'choices' => $admin->getRoles(),
            //         'choice_label' => function ($admin) {
            //             return $admin->getRoles();
            //         }
            //     ])
            ->add('password')
            ->add('firstname')
            ->add('lastname')

            ->add('workUnit')
            ->add('phoneNumber')
            ->add('isVerified', ChoiceType::class, [
                'choices'  => [
                    'Vérifié' => true,
                    'Non Vérifié' => false,
                ],
        ])
        ;

        // Data transformer
        $builder->get('roles')
        ->addModelTransformer(new CallbackTransformer(
            function ($rolesArray) {
                 // transform the array to a string
                 return count($rolesArray)? $rolesArray[0]: null;
            },
            function ($rolesString) {
                 // transform the string back to an array
                 return [$rolesString];
            }
    ));

    }




    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Admin::class,
        ]);
    }
}
